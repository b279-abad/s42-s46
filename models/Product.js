const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type: String,
		required : [true, "Product Name is required!"]
	},
	description : {
		type: String,
		required : [true, "Product Description is required!"]
	},
	price : {
		type: Number,
		required : [true, "Product Price is required!"]
	},
	isActive : {
		type: Boolean,
		default : true
	},
	createdOn : {
		type: Date,
		default : new Date("2015-03-25")
	},
	quantity : {
		type: Number,
		default : true
	},
	userOrders : [
		{
		userId : {
			type: String,
			required: [true, "UserID is required!"]
		},
		orderId : {
			type : String
		}
	}]
})

module.exports = mongoose.model("Product", productSchema);