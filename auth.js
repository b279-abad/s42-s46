const jwt = require('jsonwebtoken');

const secret = "Capstone2_API";

module.exports.createAccessToken = (user) => {
	
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
};

// Token Verification

module.exports.verify = (req, res, next) => {
	// Token is retrieved from the request header

	let token = req.headers.authorization;

	// If token received is not undefined
	if(typeof token !== "undefined"){
		console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			// If JWT is not valid
			if(error){
				return res.send({auth : "Failed!"});
			}else{
				// Allows the application to proceed with the next middleware function/callback function in the route
				// The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function
				next();
			}
		})


	}else{
		return res.send({auth : "Failed!"})
	}
}

// Token Decryption
module.exports.decode = (token) => {
	// Token is not undefined
	if (typeof token !== "undefined"){
		// Retrive only the token
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) =>{
			if(error){
				return null;
			}else{
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	}else{
		return null;
	}
}